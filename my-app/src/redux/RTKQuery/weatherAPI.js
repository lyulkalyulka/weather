import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/query";
import {createEntityAdapter} from "@reduxjs/toolkit";

const usersAdapter = createEntityAdapter()

const initialState = usersAdapter.getInitialState()

const WeatherAPI  = createApi({
    reducerPath: 'weather',
    baseQuery: fetchBaseQuery({
        baseUrl: `http://api.openweathermap.org`}),
    endpoints: builder => ({
        getData: builder.query({
            query: () => `/data`,
        }),
        getWeather: builder.query({
            query: (key) => `/data/weather/${key}`,
            transformResponse: responseData => {
             return usersAdapter.setAll(initialState, responseData)}
        })
    })
});


