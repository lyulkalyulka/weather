import {createAsyncThunk} from '@reduxjs/toolkit';
import axios from 'axios';
import {API_KEY} from '../../const/API_key';

  export const fetchWeatherAction = createAsyncThunk(
    "weather/fetch",
async (payload, { rejectWithValue,
        getState, dispatch }) => {
        try {
          const { data } = await axios.get(
         `http://api.openweathermap.org/data/2.5/weather?q=${payload}&appid=${API_KEY}`
          );
          return data;
        } catch (error) {
            if (!error?.response) {
              throw error;
            }
            return rejectWithValue(error?.response?.data);
        }
      }
  );



