import { createSlice } from '@reduxjs/toolkit';
import {fetchWeatherAction} from '../weatherAPI/weatherAPI';


  const initialState = {
    weather: {},
    favourites: false,
    name: '',
    loading: false,
    error: undefined,
    isAuthored: false,
    isFavorite: false,
  }

  const weatherSlice = createSlice({
    name: 'weather',
    initialState: initialState,
    reducers: {
      checkAutorization: (state, action) => {
        state.isAuthored = true;
      },
      logoutAutorization: (state) => {
        state.isAuthored = false;
      },
      toggleFavorite: (state) => {
        state.isFavorite = !state.isFavorite;
      },
      removeFavorite: (state) => {
        state.isFavorite = false;
      },
    },
    extraReducers: builder => {
      builder.addCase(fetchWeatherAction.pending, (state, action) => {
        state.loading = true;
      });
      builder.addCase(fetchWeatherAction.fulfilled, (state, action) => {
        state.weather = action?.payload;
        state.name = action?.payload.name;
        state.loading = false;
        state.error = undefined;
        state.isFavorite = false;
      });
      builder.addCase(fetchWeatherAction.rejected, (state, action) => {
        state.loading = false;
        state.weather = undefined;
        state.error = action?.payload;
      });
    },
});


export default weatherSlice.reducer;
export const { checkAutorization, logoutAutorization, toggleFavorite } = weatherSlice.actions;
