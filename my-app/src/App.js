import React, {Component} from 'react';
import { Route, Routes } from 'react-router-dom';
import WeatherDetails from './components/weatherDetails/weatherDetails';
import SearchInput from './components/searchInput/searchInput';
import PersonalPage from './components/personalPage/personalPage';
import RegistrationForm from './components/registrationForm/registrationForm';
import SignInForm from './components/signInForm/signInForm';
import History from './components/history/history';
import { Favorite } from './components/favorite/favorite';
import ErrorBoundary from './components/errorBoundary/errorBoundary';
import { DarkModeProvider } from './components/context/darkModeContext';

  const itemsFavorite =  JSON.parse(localStorage.getItem('favorite'));

  const arrayItemFavorite = [
    ...new Map(itemsFavorite.map((item) => [item['name'], item])).values(),
  ];

  class App extends Component{
    render() {
      return (
          <div>
            <Routes>
              <Route path='/' element={<SearchInput/>} />
              <Route path='details' element={<WeatherDetails/>}/>
              <Route path='signIn' element={<SignInForm />}/>
              <Route path='registration' element={<RegistrationForm/>}/>
              <Route path='personal' element={<PersonalPage/>}/>
              <Route path='favorite' element={<ErrorBoundary><Favorite arrayFavorite={arrayItemFavorite}/></ErrorBoundary>}/>
              <Route path='history' element={<DarkModeProvider><History/></DarkModeProvider>}/>
            </Routes>
          </div>
      )
    }
  }

export default App;
