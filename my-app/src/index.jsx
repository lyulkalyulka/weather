import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {BrowserRouter} from 'react-router-dom';
import store, {persistor} from './redux/store/store';
import App from './App';
import NavMenu from './components/navMenu/navMenu';
import './App.css';


ReactDOM.render(
  <React.StrictMode>
      <BrowserRouter>
          <Provider store={store}>
              <PersistGate persistor={persistor}>
                  <NavMenu />
                  <App/>
              </PersistGate>
          </Provider>
      </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);
