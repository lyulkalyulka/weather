import React, { useState } from 'react';

 const useInput = (initial, required, dataFromStorage) => {

    const [value, setValue] = useState(initial);
    const [error, setError] = useState(null);

    const changeValue = (e) => {
      setValue(e.target.value)
    };

    const blurReaction = (e) => {
      if(e.target.value !== dataFromStorage && !e.target.value && required ) setError('Not correct');
      // if (!e.target.value && required ) setError("Required field");
      else setError(null)
    };

    return {
      value,
      onBlur: e => blurReaction(e),
      onChange: e => changeValue(e),
      error
    };
 };

 export default useInput;
