import React, { useEffect } from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {createSelector} from '@reduxjs/toolkit';
import {fetchWeatherAction} from '../../redux/weatherAPI/weatherAPI';
import {kelvinToC} from '../../utils/temp';
import {date} from '../../utils/date';
import './weatherDetails.css';

  const WeatherDetails = () => {
    const dispatch = useDispatch();

    const select = createSelector(
        (state) => state.reducer,
        (reducer) => reducer
    )

    const state = useSelector(select);
    const {weather, name} = state;

    useEffect(() => {
      dispatch(fetchWeatherAction(name))
    },[]);

    return (
      <div>
        <div className='section-details'></div>
        <div className='weather-details'>
          <div className='weather-name'>
            {state.name}, {weather.sys.country}
          </div>
          <p className='weather-details-descr'>
            The weather condition in {state.name}, is described as :
            {weather.weather[0].description} with a temperature of {Math.ceil(kelvinToC(weather.main.temp))} °C
            and a humidity of {weather.main.humidity} %
          </p>
          <div className='weather-details-feel'>
            feels like {Math.ceil(kelvinToC(weather.main.feels_like))}°C
          </div>
          <img
            className='weather-details-icon'
            src={`https://openweathermap.org/img/wn/${weather?.weather[0].icon}@2x.png`}
            alt="weatherIcon"
          />
          <div className='weather-details-temp'>
            {Math.ceil(kelvinToC(weather?.main.temp))}°C,
          </div>
          <div className='weather-details-temp-min'>
            min {Math.ceil(kelvinToC(weather.main.temp_min))}°C
          </div>
          <div className='weather-details-temp-max'>
            max {Math.ceil(kelvinToC(weather.main.temp_max))}°C
          </div>
          <div className='weather-details-date'>{date()}</div>
        </div>
      </div>
    )
  };

export default WeatherDetails;
