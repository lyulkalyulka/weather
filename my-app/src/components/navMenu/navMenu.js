import React from 'react';
import {Link, useNavigate} from 'react-router-dom';
import {useDispatch, useSelector} from 'react-redux';
import {logoutAutorization} from '../../redux/slices/weatherSlices';
import logo from '../../assets/logo.jpg'
import './navMenu.css';

  const Logo = () => {
    return (
      <img src={logo}/>
    )
  };


  const NavMenu = () => {

    const dispatch = useDispatch();
    const isAuthored = useSelector((store) => store.reducer.isAuthored);

    const navigate = useNavigate();

    const logout = () => {
      dispatch(logoutAutorization());
      navigate('/')
    }
      let name = localStorage.getItem('name');
      name = name.replace(/"/g, '');

    return (
      <div className='nav'>
        <div className='logo'>
          <Link to='/'><Logo/></Link>
        </div>
        <div className='button-nav'>
        <button className='signIn'>
          {!isAuthored && <Link to='/signIn'>signIn</Link>}
        </button>
        <button className='registration'>
          {!isAuthored && <Link to='/registration'>Registration</Link>}
        </button>
        </div>
          {isAuthored &&
              (<div className='logout' onClick={logout}>logout <span>{name}</span></div>)
          }
      </div>
    )
};

export default NavMenu;
