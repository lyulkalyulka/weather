import React, { useState} from 'react';
import { useNavigate  } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import useInput from '../../hook/useInput';
import {checkAutorization} from '../../redux/slices/weatherSlices';
import './form.css';

  const SignInForm = () => {

    const [namelog, setNamelog] = useState("");
    const [passwordlog, setPasswordlog] = useState("");

    const name = JSON.parse(localStorage.getItem('name'));
    const pass = JSON.parse(localStorage.getItem('password'));

    const login = useInput('', true, name);
    const password = useInput('', true, pass);

    const navigate = useNavigate();
    const dispatch = useDispatch();

    const handleLogin = (e) => {
      e.preventDefault();
      {if (name === namelog && pass === passwordlog) {
        dispatch(checkAutorization());
        navigate('/personal')}
      }
    };

    const loginChange = (e) => {
      setNamelog(e.target.value);
    };

    const passwordChange = (e) => {
      setPasswordlog(e.target.value);
    };

    return (
      <div
          className='container'>
        <form
            className='form'
            onSubmit={handleLogin}>
          <h1>User</h1>
            <input
                className='form-text'
                {...login} value={namelog}
                 type="text"
                 onChange={e => loginChange(e)}
                 placeholder="Login..." />
              {login.error && <span style={{color: 'red'}}>{login.error}</span>}
            <br/>
            <input
                className='form-password'
                {...password} type='password'
                 value={passwordlog}
                 onChange={e => passwordChange(e)}
                 placeholder="Password..."/>
            <br/>
              {password.error && <span style={{color: 'red'}}>{password.error}</span>}
            <br/>
            <input
                className='form-submit'
                type="submit" value="Submit"/>
        </form>
      </div>
    );
  };

export default SignInForm;
