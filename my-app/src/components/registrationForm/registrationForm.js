import React, {useState} from 'react';
import {useNavigate} from 'react-router-dom';
import useInput from '../../hook/useInput';
import '../signInForm/form.css';

  const RegistrationForm = () => {

    const [name, setName] = useState("");
    const [password, setPassword] = useState("");

    const login = useInput('', true);
    const pass = useInput('', true);

    const navigate = useNavigate();

    const handleFormSubmit = (e) => {
      e.preventDefault();
      localStorage.setItem('name', JSON.stringify(name));
      localStorage.setItem('password', JSON.stringify(password));
      {name && password ? navigate('/personal') : navigate('/')}
    };

    const nameChange = (e) => {
      setName(e.target.value)
    };

    const passwordChange = (e) => {
      setPassword(e.target.value)
    };

    return (
      <div
        className='container'>
        <form
          className='form'
          onSubmit={handleFormSubmit}>
        <h1>User Registration</h1>
          <input
            className='form-text'
            {...login} value={name}
            type='text'
            onChange={e => nameChange(e)}
            placeholder='Login...' />
            {login.error && <span style={{color: 'red'}}>{login.error}</span>}
            <br/>
          <input
              className='form-password'
              {...pass} value={password}
              type='password'
              onChange={e => passwordChange(e)}
              placeholder='Password...'/>
              <br />
              {pass.error && <span style={{color: 'red'}}>{pass.error}</span>}
              <br/>
          <input
              className='form-submit'
              type='submit'
              value='Submit' />
        </form>
      </div>
    );
  };

export default RegistrationForm;
