import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import './personalPage.css';

 export default class PersonalPage extends Component{
    render() {
      return (
        <div className='personalPage'>
            <Link to='/favorite'>favorite</Link>
            <Link to='/history'>history</Link>
        </div>
      )
    }
  };
