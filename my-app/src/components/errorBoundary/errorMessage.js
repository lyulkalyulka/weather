import img from '../../assets/gif.gif';
import './errorMessage.css';

  const ErrorMessage = () => {
    return (
      <img className='errorMessage' src={img} alt="Error"/>
    )
  };

export default ErrorMessage;
