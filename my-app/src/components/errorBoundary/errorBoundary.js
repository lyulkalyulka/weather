import React from 'react';
import ErrorMessage from './errorMessage';

 export default class ErrorBoundary extends React.Component {
   constructor(props) {
     super(props);
     this.state = {errorFound: false};
   }

   static getDerivedStateFromError() {
     return {errorFound: true};
   }

    render() {
      if (this.state.errorFound) {
       return <div><ErrorMessage/></div>
    }
      return this.props.children;
    }
 }

