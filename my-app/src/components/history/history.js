import React, {useContext} from 'react';
import {DarkModeContext} from '../context/darkModeContext';
import Content from '../context/content';
import LightSwitch from '../context/lightSwitch';
import './history.css';

const arrayFromStorage =  JSON.parse(localStorage.getItem('history'));

  const History = () => {
    const {darkMode} = useContext(DarkModeContext);

      return (
        <div
          className={darkMode ? `history history-dark` : `history history-light`}>
          <Content arrayFromStorage={arrayFromStorage}/>
          <LightSwitch/>
        </div>
      )
  };

  export default History;
