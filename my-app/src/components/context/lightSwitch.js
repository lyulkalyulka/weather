import React, {useContext} from 'react';
import {DarkModeContext} from './darkModeContext';
import off from '../../assets/lightswitch-off.png';
import on from '../../assets/lightswitch-on.png';
import './context.css';

  const LightSwitch = () => {
    const {darkMode, toggleDarkMode} = useContext(DarkModeContext);

    const handleClick = () => {
      toggleDarkMode();
    };
    return (
      <div className='lightswitch'>
        <img src={darkMode ?
          off : on}
          alt="lightswitch on" onClick={handleClick}/>
      </div>
    )
  };

export default LightSwitch;
