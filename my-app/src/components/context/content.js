import React, {useContext} from 'react';
import {DarkModeContext} from './darkModeContext';
import './context.css';

 const Content = ({arrayFromStorage}) => {
    const {darkMode} = useContext(DarkModeContext);

     const getListItem = arrayFromStorage.map(function (key) {
       return <li key={key}>{key}</li>
     });

    const changeContentProvider = () => {
      if (darkMode) {
        return (
          <>
           <h1>Search History</h1>
           <ul>{getListItem}</ul>
          </>
        )
      }
        return (
          <>
            <h1>Search History</h1>
            <ul>{getListItem}</ul>
          </>
        )
    }
      return (
        <div className={darkMode ? `content content-dark` : `content`}>
          {changeContentProvider()}
        </div>
      )
  }

export default Content;
