import React, {useState} from 'react';
import PlacesAutocomplete from 'react-places-autocomplete';
import {useDispatch, useSelector} from 'react-redux';
import {fetchWeatherAction} from '../../redux/weatherAPI/weatherAPI';
import Weather from '../weather/weather';
import './searchInput.css';

  const SearchInput = () => {

    const dispatch = useDispatch();

    const [address, setAddress] = useState('');
      const state = useSelector(state => state.reducer);
      const {name} = state;

    const historyChange = () => {
      dispatch(fetchWeatherAction(address));
      const data = JSON.parse(localStorage.getItem('history'));
      const history = [...data, name]
      localStorage.setItem('history', JSON.stringify(history));
    }

    return (
      <div>
        <PlacesAutocomplete
          value={address}
          onChange={setAddress}>
            {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
              <div className='search'>
                <form className='auto-search'>
                  <input
                    className='search-input'
                    value={address}
                    onChange={e => setAddress(e.target.value)}
                    {...getInputProps({ placeholder: 'Type address'})} />
                      <button
                        className='search-button'
                        onClick={() => historyChange()}
                        type='button'>
                        search
                      </button>
                </form>
                <div>
                  {loading ? <div>...loading</div> : null}
                  {suggestions.map(suggestion => {
                    return (
                      <div {...getSuggestionItemProps(suggestion)}>
                        {suggestion.description}
                      </div>
                    );
                })}
                </div>
                  <Weather/>
                </div>
              )}
        </PlacesAutocomplete>
    </div>
    );
  };

export default SearchInput;
