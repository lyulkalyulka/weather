import React, { useEffect} from 'react';
import {Link} from 'react-router-dom';
import {useDispatch, useSelector} from 'react-redux';
import {fetchWeatherAction} from '../../redux/weatherAPI/weatherAPI';
import {toggleFavorite} from '../../redux/slices/weatherSlices';
import {weatherSlice} from '../../redux/slices/weatherSlices';
import {createSelector} from '@reduxjs/toolkit';
import {date} from '../../utils/date';
import {kelvinToC} from '../../utils/temp';
import emptyLike from '../../assets/like.png';
import redLike from '../../assets/like_red.png';
import './weather.css';

  const Weather = () => {

    const dispatch = useDispatch();

    const select = createSelector(
        (state) => state.reducer,
        (reducer) => reducer
    )
    const state = useSelector(select);
    const {weather, name, isFavorite} = state;

   const addToFavorite = () => {
     dispatch(toggleFavorite());

     const data = JSON.parse(localStorage.getItem('favorite'));
     const itemFavorite = [...data,{name,weather}]
     localStorage.setItem('favorite', JSON.stringify(itemFavorite));
    }

    useEffect(() => {
      dispatch(fetchWeatherAction(name))
    },[name]);

    return (
      <div className='section'>
        <div className='weather'>
          <img
            className='weather-icon'
            src={`https://openweathermap.org/img/wn/${weather?.weather[0].icon}@2x.png`}
            alt="weatherIcon"
            />
            <div className='weather-city-temp'>
              {Math.ceil(kelvinToC(weather?.main.temp))}°C
            </div>
            <div className='weather-city-name'>
              {weather?.name}, {weather?.sys?.country}
            </div>
            <div className='weather-city-figcaption'>
              {weather?.weather[0].main}
            </div>{" "}
            <div className='weather-city-date'>{date()}</div>
            <Link to='details'>
              <button
                  className='weather-city-details'
                  type='button'
                  onClick={() => dispatch(fetchWeatherAction(name))}>details
              </button>
            </Link>
              <button
                  className='weather-city-favorite'
                  onClick={() => addToFavorite()}>
                  {isFavorite ? <img className='weather-city-like-img' src={redLike} alt='like'/> :
                    <img className='weather-city-like-img' src={emptyLike} alt='like'/> }
                </button>
        </div>
    </div>
    )
}

export default Weather;
