import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {kelvinToC} from '../../utils/temp';
import {date} from '../../utils/date';
import './favorite.css'

  export class Favorite extends Component{
    constructor(arrayFavorite) {
      super(arrayFavorite);
    }

    render() {
      const data =  this.props.arrayFavorite.map(function ({name, weather}) {
        return (
          <div className='section-favorite'>
            <div className='weather'>
              <img
                className='weather-icon'
                src={`https://openweathermap.org/img/wn/${weather.weather[0].icon}@2x.png`}
                alt="iconWeather"
              />
              <span className='weather-city-name'>{name}, {weather.sys.country}</span>
              <span className='weather-city-figcaption'>{weather.weather[0].main}</span>
              <span className='weather-city-temp'>{Math.ceil(kelvinToC(weather.main.temp))}°C</span>
              <span className='weather-city-date'>{date()}</span>
            </div>
          </div>
        )
      });
      return (
          <div className='section-favorite'>
            {data}
          </ div>
      )
    }
  };

  Favorite.propTypes = {
    arrayFavorite:PropTypes.array
  };