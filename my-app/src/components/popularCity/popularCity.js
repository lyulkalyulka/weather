import React, {useEffect, useState} from "react";
import './popularCity.css';
import paris from './paris.png';
import london from './london.png';
import dubai from './dubai.png';
import new_york from './newyork.png';
import {useDispatch, useSelector} from "react-redux";
import {fetchWeatherAction} from "../../redux/slices/weatherSlices";


const PopularCity = () => {

    const dispatch = useDispatch();

    return (
        <div className=''>
            <h1 className='title'>Check the weather in most popular cities in the world
            </h1>
            <div className="inner">
                <div className="inner__picture">
                    <img src={new_york} alt="newyork"/>
                        <button className='popular-city'
                            type='button'
                            onClick={() => dispatch(fetchWeatherAction('new york'))}
                        >New York</button>
                </div>
                <div className="inner__picture">
                    <img src={london} alt="london"/>
                        <button className='popular-city'
                            type='button'
                            onClick={() => dispatch(fetchWeatherAction('london'))}
                        >London</button>
                </div>
                <div className="inner__picture">
                    <img src={dubai} alt="dubai"/>
                        <button className='popular-city'
                            type='button'
                            onClick={() => dispatch(fetchWeatherAction('dubai'))}
                        >Dubai</button>
                </div>
                <div className="inner__picture">
                    <img src={paris} alt="paris"/>
                        <button className='popular-city'
                            type='button'
                            onClick={() => dispatch(fetchWeatherAction('paris'))}
                        >Paris</button>
                </div>
            </div>
        </div>
    )
}

export default PopularCity;