  const month = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];


export const date = () => {
    const d = new Date();
    const date = month[d.getMonth()];
    return `${new Date().getDate()} ${date} ${d.getFullYear()}`;
}